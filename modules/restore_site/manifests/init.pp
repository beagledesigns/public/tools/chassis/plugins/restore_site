# restore_site
#
# Chassis plugin to restore the wp-content folder and database from a wordpress site backup
#
# @param [Array] config  The chassis configuration
class restore_site (
  $config
) {

  # Default settings
  $defaults = {
    'restore_site' => {
      'source_site' => 'backup/site.tgz',
      'source_db'   => 'backup/database.sql.gz',
      'restore'     => 'once',
    },
  }

  # Allow override from config.yaml
  $options = deep_merge($defaults, $config)

  case $options['restore_site']['restore'] {
    'once':   {
      class { 'restore_site::restore':
        options   => $options,
        onrefresh => true,
      }
    }
    'always': {
      class { 'restore_site::restore':
        options   => $options,
        onrefresh => false,
      }
    }
    default: { notify { 'restore_site disabled': } }
  }
}
