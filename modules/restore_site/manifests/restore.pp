# restore_site::restore
#
# Restore the wp-content folder and database from a wordpress site backup
#
# @param [Array] options The options to use for the restore
# @param [Boolean] onrefresh Whether to 
class restore_site::restore (
  $options,
  $onrefresh = true
) {
  $user = $options['admin']['user']
  $password = $options['admin']['password']
  $email = $options['admin']['email']
  $hostname = $options['hosts'][0]
  $location = $options['mapped_paths']['base']
  $db_path = "${options['mapped_paths']['base']}/${options['restore_site']['source_db']}"
  $site_backup = "${options['mapped_paths']['base']}/${options['restore_site']['source_site']}"
  $sitefolder = regsubst($options['paths']['wp'], "${options['mapped_paths']['base']}/", '')
  $homeurl = "http://${hostname}"
  $siteurl = "${homeurl}/${sitefolder}"
  $plugins_to_disable = join($options['restore_site']['plugins_to_disable'], ' ')
  $db_prefix = $options['database']['prefix']

  exec { '/usr/bin/wp db drop --yes':
    cwd     => $location,
    user    => 'vagrant',
    command => '/usr/bin/wp db drop --yes',
    require => [
      Class['wp::cli'],
      Chassis::Content[ $options['hosts'][0] ],
    ],
  }

  exec { '/usr/bin/wp db create':
    cwd     => $location,
    user    => 'vagrant',
    command => '/usr/bin/wp db create',
    require => [
      Class['wp::cli'],
      Exec['/usr/bin/wp db drop --yes'],
    ],
  }

  exec { "/bin/gunzip < \"${db_path}\" | /usr/bin/wp db import -":
    cwd     => $location,
    user    => 'vagrant',
    command => "/bin/gunzip < \"${db_path}\" | /usr/bin/wp db import -",
    require => [
      Class['wp::cli'],
      Exec['/usr/bin/wp db create'],
    ],
  }

  exec { "wp option set home \"${homeurl}\"":
    cwd     => $location,
    user    => 'vagrant',
    command => "wp option set home \"${homeurl}\"",
    unless  => "test \"$(wp db query 'SELECT option_value FROM ${db_prefix}options WHERE option_name=\"home\"' --batch --skip-column-names)\" = \"${homeurl}\" 2>/dev/null",
    path    => ['/usr/bin', '/usr/sbin'],
    require => [
      Class['wp::cli'],
      Exec["/bin/gunzip < \"${db_path}\" | /usr/bin/wp db import -"],
    ],
  }

  exec { "wp option set siteurl \"${siteurl}\"":
    cwd     => $location,
    user    => 'vagrant',
    command => "wp option set siteurl \"${siteurl}\"",
    unless  => "test \"$(wp db query 'SELECT option_value FROM ${db_prefix}options WHERE option_name=\"siteurl\"' --batch --skip-column-names)\" = \"${siteurl}\" 2>/dev/null",
    path    => ['/usr/bin', '/usr/sbin'],
    require => [
      Class['wp::cli'],
      Exec["/bin/gunzip < \"${db_path}\" | /usr/bin/wp db import -"],
    ],
  }

  exec { "wp search-replace \"$(wp db query 'SELECT option_value FROM ${db_prefix}options WHERE option_name=\"home\"' --batch --skip-column-names)\" \"${homeurl}\"":
    cwd     => $location,
    user    => 'vagrant',
    command => "wp search-replace \"$(wp db query 'SELECT option_value FROM ${db_prefix}options WHERE option_name=\"home\"' --batch --skip-column-names)\" \"${homeurl}\"",
    path    => ['/usr/bin', '/usr/sbin'],
    require => [
      Class['wp::cli'],
      Exec["/bin/gunzip < \"${db_path}\" | /usr/bin/wp db import -"],
    ],
    before  => [
      Exec["wp option set home \"${homeurl}\""],
    ],
  }

  # Extract wp-content backup
  file { '/vagrant/wp-content':
    ensure => directory,
    mode   => '0755',
  }

  exec { "tar -xf ${site_backup}":
    cwd     => '/vagrant/wp-content',
    command => "tar -xf ${site_backup} site/wp-content --strip-components 2",
    path    => ['/bin'],
    require => File['/vagrant/wp-content'],
    timeout => 0,
  }

  # Disable plugins
  exec { "/usr/bin/wp plugin deactivate ${plugins_to_disable}":
    cwd     => $location,
    user    => 'vagrant',
    command => "/usr/bin/wp plugin deactivate ${plugins_to_disable}",
    returns => [ 0, 1 ],
    require => [
      Class['wp::cli'],
      Exec["/bin/gunzip < \"${db_path}\" | /usr/bin/wp db import -"],
    ],
  }

  exec { "wp user delete ${user} --yes":
    cwd     => $location,
    user    => 'vagrant',
    command => "wp user delete ${user} --yes",
    onlyif  => "wp user get ${user} 2>/dev/null",
    path    => ['/usr/bin', '/usr/sbin'],
    require => [
      Class['wp::cli'],
      Exec["/bin/gunzip < \"${db_path}\" | /usr/bin/wp db import -"],
    ],
  }

  exec { "wp user create ${user} ${email} --role=administrator --user_pass=${password}":
    cwd     => $location,
    user    => 'vagrant',
    command => "wp user create ${user} ${email} --role=administrator --user_pass=${password}",
    path    => ['/usr/bin', '/usr/sbin'],
    require => [
      Class['wp::cli'],
      Exec["wp user delete ${user} --yes"],
    ],
  }
}
