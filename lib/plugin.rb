module SiteRestore
	class RestoreSite
		def initialize(app, env)
			@app = app
			@env = env
		end

		def call(env)
			@env = env
			env[:machine].ui.info "***** AJXB - RestoreSite called *****"
			@app.call(env)
		end
	end

	class Plugin < Vagrant.plugin("2")
		name "Site Import"

		include Vagrant::Action
		include Chassis

		config = Chassis::load_config

		action_hook(:restore_site, :machine_action_provision) do |hook|
			hook.append SiteRestore::RestoreSite
		end
	end
end
